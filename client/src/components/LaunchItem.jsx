import classname from 'classname';
import Moment from 'react-moment';
import {Link} from 'react-router-dom';

function LaunchItem({launch}) {
  const {
    flight_number: flightNumber,
    mission_name: missionName,
    launch_date_local: launchDateLocal,
    launch_success: launchSuccess,
  } = launch;

  return (
    <div className="card card-body mb-3">
      <div className="row">
        <div className="col-md-9">
          <h4>
            Mission:{' '}
            <span
              className={classname({
                'text-success': launchSuccess,
                'text-danger': !launchSuccess,
              })}
            >
              {missionName}
            </span>
          </h4>
          <p>
            Date: <Moment format="YYYY-MM-DD HH:mm">{launchDateLocal}</Moment>
          </p>
        </div>
        <div className="col-md-3">
          <Link to={`/launch/${flightNumber}`} className="btn btn-secondary">
            Launch Details
          </Link>
        </div>
      </div>
    </div>
  );
}

export default LaunchItem;
