import React from 'react';
import LaunchItem from './LaunchItem';
import {gql} from '@apollo/client';
import {Query} from '@apollo/client/react/components';
import MissionKey from './MissionKey';

const LAUNCHES_QUERY = gql`
  query LaunchesQuery {
    launches {
      flight_number
      mission_name
      launch_date_local
      launch_success
    }
  }
`;

const Launches = () => {
  return (
    <>
      <h1 className="display-4 my-3">Launches</h1>
      <MissionKey />
      <Query query={LAUNCHES_QUERY}>
        {({loading, error, data}) => {
          console.log(data);
          if (loading) return <h4>Loading...</h4>;
          if (error) console.log(error);

          return (
            <>
              {data.launches?.map((el, index) => (
                <LaunchItem key={index} launch={el} />
              ))}
            </>
          );
        }}
      </Query>
    </>
  );
};

export default Launches;
