const express = require('express');
import {createHandler} from 'graphql-http/lib/use/express';
// const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema');
const cors = require('cors');
const path = require('path');

// Create a express instance serving all methods on `/graphql`
// where the GraphQL over HTTP express request handler is
const app = express();

app.use(cors());

app.all('/graphql', createHandler({schema}));

// app.use(
//   'localhost:3000/graphql',
//   graphqlHTTP({
//     schema: schema,
//     graphiql: true,
//   })
// );

app.use(express.static('public'));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

const PORT = process.env.PORT || 4000;

app.listen({port: PORT}, () => console.log(`Server started on port ${PORT}`));
